from layers.Residual import Residual
import torch.nn as nn
import math
import ref

"""
https://github.com/aleju/papers/blob/master/neural-nets/Stacked_Hourglass_Networks_for_Human_Pose_Estimation.md
https://stackoverflow.com/questions/41211691/tensorflow-implementation-is-2x-slower-than-the-torchs-one

"""


class Hourglass(nn.Module):
  def __init__(self, n, nModules, nFeats):
    super(Hourglass, self).__init__()
    self.n = n # n is number of hourglass
    self.nModules = nModules # = 2
    self.nFeats = nFeats 
    
    _up1_, _low1_, _low2_, _low3_ = [], [], [], []
    for j in range(self.nModules):
      _up1_.append(Residual(self.nFeats, self.nFeats))
    self.low1 = nn.MaxPool2d(kernel_size = 2, stride = 2) # Reduce dimension of layer by 2
    for j in range(self.nModules):
      _low1_.append(Residual(self.nFeats, self.nFeats))
    
    if self.n > 1:
      self.low2 = Hourglass(n - 1, self.nModules, self.nFeats)
    else:
      for j in range(self.nModules):
        _low2_.append(Residual(self.nFeats, self.nFeats))
      self.low2_ = nn.ModuleList(_low2_)
    
    for j in range(self.nModules):
      _low3_.append(Residual(self.nFeats, self.nFeats))
    
    self.up1_ = nn.ModuleList(_up1_)
    self.low1_ = nn.ModuleList(_low1_)
    self.low3_ = nn.ModuleList(_low3_)
    
    self.up2 = nn.Upsample(scale_factor = 2)
    
  def forward(self, x):
    up1 = x
    for j in range(self.nModules):
      up1 = self.up1_[j](up1)
    
    low1 = self.low1(x)
    for j in range(self.nModules):
      low1 = self.low1_[j](low1)
    
    if self.n > 1:
      low2 = self.low2(low1)
    else:
      low2 = low1
      for j in range(self.nModules):
        low2 = self.low2_[j](low2)
    
    low3 = low2
    for j in range(self.nModules):
      low3 = self.low3_[j](low3)
    up2 = self.up2(low3)
    
    return up1 + up2

class HourglassNet3D(nn.Module):
  def __init__(self, nStack, nModules, nFeats, nRegModules):
    super(HourglassNet3D, self).__init__()
    self.nStack = nStack # = 2
    self.nModules = nModules # = 4
    self.nFeats = nFeats # = 256
    self.nRegModules = nRegModules # = 2
     # This will reduce the size from 3x 256x256 to 64x 128x128
    self.conv1_ = nn.Conv2d(3, 64, bias = True, kernel_size = 7, stride = 2, padding = 3)
    self.bn1 = nn.BatchNorm2d(64)
    self.relu = nn.ReLU(inplace = True)
    # 128x 128x128
    self.r1 = Residual(64, 128)
    # This will reduce the size from 128x 128x128 to 128x 64x64
    self.maxpool = nn.MaxPool2d(kernel_size = 2, stride = 2)
    self.r4 = Residual(128, 128)
    # nFeats is the number of channels, but considered number of (super) features in hourglass
    # 256x 64x64
    self.r5 = Residual(128, self.nFeats)
    
    _hourglass, _Residual, _lin_, _tmpOut, _ll_, _tmpOut_, _reg_ = [], [], [], [], [], [], []
    # There are 2 Hourglass stacking on eachother
    for i in range(self.nStack):
      # Each Hourglass has 4 (recursive) calls 
      _hourglass.append(Hourglass(4, self.nModules, self.nFeats))
      for j in range(self.nModules):
        _Residual.append(Residual(self.nFeats, self.nFeats))
      # lin stands for Linear layer (e.g FC layer). They use 1-con2d instead of Linear, following by BatchNorm2D
      # Because each feature is a 2D matrix so we cannot use nn.Linear (it is for flatten input layer)
      # The output here is still 256x 64x64 
      lin = nn.Sequential(nn.Conv2d(self.nFeats, self.nFeats, bias = True, kernel_size = 1, stride = 1), 
                          nn.BatchNorm2d(self.nFeats), self.relu)
      _lin_.append(lin)
      # _tmpOut: Temporary loss to learn from first Hourglass
      # 1-convolution to produce 16x 64x64
      _tmpOut.append(nn.Conv2d(self.nFeats, ref.nJoints, bias = True, kernel_size = 1, stride = 1))

      # We skip the temp heat map with another linear layer (1-conv) 256x 64x64
      _ll_.append(nn.Conv2d(self.nFeats, self.nFeats, bias = True, kernel_size = 1, stride = 1))
      # _tmpOut_: Upscale from nJoints to nFeats
      _tmpOut_.append(nn.Conv2d(ref.nJoints, self.nFeats, bias = True, kernel_size = 1, stride = 1))

      # Then we sum up every thing: Original Input + Last Linear Layer + Upscaled from Temp Heatmap
      # The carried on feature is  256x 64x64

    # This reg is 3D optimizer
    for i in range(4):
      for j in range(self.nRegModules):
        _reg_.append(Residual(self.nFeats, self.nFeats))
        
    self.hourglass = nn.ModuleList(_hourglass)
    self.Residual = nn.ModuleList(_Residual)
    self.lin_ = nn.ModuleList(_lin_)
    self.tmpOut = nn.ModuleList(_tmpOut)
    self.ll_ = nn.ModuleList(_ll_)
    self.tmpOut_ = nn.ModuleList(_tmpOut_)
    self.reg_ = nn.ModuleList(_reg_)

    # After 4 regulated layers the feature size is reduced to  256x 4x4  due to 4 maxpool
    # Then it is flatten so the feature size is 256x 4x4 = 4 * 4 * nFeats
    # And finally it goes through FC layer into 16 joints depth (in mm?)
    self.reg = nn.Linear(4 * 4 * self.nFeats, ref.nJoints)
    
    # We can take last carried on value (x+ll+upscaled of tmpout), run through 4 regmodule with maxpool to produce exactly 256x 4*4
    # Then we add up with the last layer to produce another 256x 4x4
    # And flatten it to run through another linear layer
    # Then have a cross entropy loss with 2 logits
    # Steps to implement:
    # 
    # - How do I freeze gradient of all layers?
    # - How do I remove the last reg layers?
    # - Prepare original data, then add new data with correct pose. Edit the original data with new label. Load and test.
    # - Feed in and train

  def forward(self, x):
    x = self.conv1_(x)
    x = self.bn1(x)
    x = self.relu(x)
    x = self.r1(x)
    x = self.maxpool(x)
    x = self.r4(x)
    x = self.r5(x)
    # At this point, we will have nFeats x 64x64 features. So a 64x64 patch is considered a super feature
    out = []
    
    for i in range(self.nStack):
      hg = self.hourglass[i](x)
      ll = hg
      for j in range(self.nModules):
        ll = self.Residual[i * self.nModules + j](ll)
      ll = self.lin_[i](ll)
      tmpOut = self.tmpOut[i](ll)
      # all out element except last one are temporary heat maps
      out.append(tmpOut)
      
      ll_ = self.ll_[i](ll)
      tmpOut_ = self.tmpOut_[i](tmpOut)
      # to continue: residual from x, upscaled from temp heat map, and a Fully Connected layer from the last layer
      x = x + ll_ + tmpOut_
    
    # nRegModules = 2
    for i in range(4):
      for j in range(self.nRegModules):
        x = self.reg_[i * self.nRegModules + j](x)
      # max pool every nRegModules (= 2)
      # which means reduce dimensions by 2 every outer loop
      # which means reduce dimensions by total of 2^4= 16 (for each dim) by the end of this loop
      x = self.maxpool(x)
    # View in torch means reshape in numpy  
    # The below line is to make sure x is 2-dim, with the first dim unchanged
    # This is flattening of the last 2-dimensional layer
    x = x.view(x.size(0), -1) 

    # Reg is a Fully Connected layer that connect from a already-flatten layer to the joint
    reg = self.reg(x)
    # last output is 3d prediction
    out.append(reg)
    
    return out

