from hg_3d import HourglassNet3D
from layers.Residual import Residual
import torch.nn as nn
import math
import ref
import torch

class PoseNet(nn.Module):
  def __init__(self, hg):
    super(PoseNet, self).__init__()
    
    self.nStack = hg.nStack # = 2
    self.nModules = hg.nModules # = 4
    self.nFeats = hg.nFeats # = 256
    self.nRegModules = hg.nRegModules # = 2
    # This will reduce the size from 3x 256x256 to 64x 128x128
    self.conv1_ = hg.conv1_
    self.bn1 = hg.bn1
    self.relu = hg.relu
    # 128x 128x128
    self.r1 = hg.r1
    # This will reduce the size from 128x 128x128 to 128x 64x64
    self.maxpool = hg.maxpool
    self.r4 = hg.r4
    # nFeats is the number of channels, but considered number of (super) features in hourglass
    # 256x 64x64
    self.r5 = hg.r5
    
    self.hourglass = hg.hourglass
    self.Residual = hg.Residual
    self.lin_ = hg.lin_
    self.tmpOut = hg.tmpOut
    self.ll_ = hg.ll_
    self.tmpOut_ = hg.tmpOut_
    self.reg_ = hg.reg_
    self.reg = hg.reg

    # This set of modules will reduce the dim from 64x64 to 16x16 thanks to a following maxpool layer
    _tfer2d  = []
    for i in range(2):
      _tfer2d.append(Residual(ref.nJoints, ref.nJoints))
    self.tfer2d = nn.ModuleList(_tfer2d)

    # Bring it down 16 times
    self._fc = nn.Linear(4 * 4 * self.nFeats, self.nFeats)

    # Apply dropout
    self.drop = nn.Dropout(p=ref.drop_rate)

    # This is the final FC
    # We stack 2 layers: 2D features and 3D features on top. Each layer has 4 * 4 * 256 features. Two layers had 8192
    # self.fc = nn.Linear(4 * 4 * self.nFeats * 2, 2)
    self.fc = nn.Linear(self.nFeats, 2)


  def forward(self, x):
    x = self.conv1_(x)
    x = self.bn1(x)
    x = self.relu(x)
    x = self.r1(x)
    x = self.maxpool(x)
    x = self.r4(x)
    x = self.r5(x)
    # At this point, we will have nFeats x 64x64 features. So a 64x64 patch is considered a super feature
    out = []
    
    for i in range(self.nStack):
      hg = self.hourglass[i](x)
      ll = hg
      for j in range(self.nModules):
        ll = self.Residual[i * self.nModules + j](ll)
      ll = self.lin_[i](ll)
      tmpOut = self.tmpOut[i](ll)
      # all out element except last one are temporary heat maps
      out.append(tmpOut)
      
      ll_ = self.ll_[i](ll)
      tmpOut_ = self.tmpOut_[i](tmpOut)
      # to continue: residual from x, upscaled from temp heat map, and a Fully Connected layer from the last layer
      x = x + ll_ + tmpOut_
    
    tfer = tmpOut
    for i in range(2):
      tfer = self.tfer2d[i](tfer)
      tfer = self.maxpool(tfer)

    tfer = tfer.view(tfer.size(0), -1) #flattening

    # nRegModules = 2
    for i in range(4):
      for j in range(self.nRegModules):
        x = self.reg_[i * self.nRegModules + j](x)
      # max pool every nRegModules (= 2)
      # which means reduce dimensions by 2 every outer loop
      # which means reduce dimensions by total of 2^4= 16 (for each dim) by the end of this loop
      x = self.maxpool(x)
    # View in torch means reshape in numpy  
    # The below line is to make sure x is 2-dim, with the first dim unchanged
    # This is flattening of the last 2-dimensional layer
    x = x.view(x.size(0), -1)

    # Reg is a Fully Connected layer that connect from a already-flatten layer to the joint
    reg = self.reg(x)
    # last output is 3d prediction
    out.append(reg)

    # Begin transfer

    tfer = self.drop(tfer)
    tfer = self._fc(tfer)
    x = self.drop(x)
    x = self._fc(x)
    x = x + tfer
    x = self.drop(x)

    # Final layer is a dense layer from nFeat to 2 logits
    x = self.fc(x)

    # tfer = torch.cat([tfer, x], dim=1)
    # fc = self.fc(tfer)

    out.append(x)
    return out   

