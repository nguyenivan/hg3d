import sys
import torch
from opts import opts
import ref
from utils.debugger import Debugger
from utils.eval import getPreds
import cv2
import numpy as np
from torchviz import make_dot

# pip install git+https://github.com/szagoruyko/pytorchviz



from functools import partial
# import pickle
# pickle.load = partial(pickle.load, encoding="latin1")
# pickle.Unpickler = partial(pickle.Unpickler, encoding="latin1")

def main():
  opt = opts().parse()
  if opt.loadModel != 'none':
    model = torch.load(opt.loadModel).cuda()
  else:
    model_file = 'hgreg-3d.pth'
    model = torch.load('hgreg-3d.pth').cuda()
    # model = torch.load(model_file, map_location=lambda storage, loc: storage, pickle_module=pickle)
  
  if opt.demo:
    img = cv2.imread(opt.demo)
  else:
    image_file = '../images/mpii_2.png'
    img = cv2.imread(image_file)
    
  input = torch.from_numpy(img.transpose(2, 0, 1)).float() / 256.
  input = input.view(1, input.size(0), input.size(1), input.size(2))
  input_var = torch.autograd.Variable(input).float().cuda()
  output = model(input_var)
  reg = output[2]
  print ('RENDERING... ')
  make_dot(reg, params=dict(model.named_parameters()))
  # dot = make_dot(reg, params=dict(model.named_parameters()))
  # dot.format = 'svg'
  # dot.render()
  print ('DONE!')


if __name__ == '__main__':
  main()
