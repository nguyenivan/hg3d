#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import gui3d
import mh
import material
import gui
import log
import os
from cStringIO import StringIO
from core import G
from progress import Progress
import image_operations as imgop
from guirender import RenderTaskView
import projection
import image
import numpy as np
from time import sleep
from threading import Thread

DEBUG = True
REPEAT = 10
HR = [-45,45]
VR = [-45,45]
RANDOM_UP = True
FILE_BASE = 'render'
FILE_EXT = 'png'

class RunThread(Thread):
    def __init__(self, app, dest):
        Thread.__init__(self)
        self.app = app
        if not os.path.exists(dest):
            os.makedirs(dest)
        self.dest = dest
    def progress(self, progress, status=None):
        pass
    
    def run(self):
        org_rot = self.app.modelCamera.getRotation()
        org_up = self.app.modelCamera.getUp()
        for i in range(REPEAT):
            hor = np.random.randint(HR[0], HR[1]+1)
            ver = np.random.randint(VR[0], VR[1]+1)
            self.app.modelCamera.setRotation ((hor, ver))
            if RANDOM_UP:
                up = self.random_three_vector()
                self.app.modelCamera.setUp(up)
            img = self.render()
            path = os.path.join(self.dest, 
                '{}{}.{}'.format(FILE_BASE, i, FILE_EXT) )
            img.save(path)
            log.message('Save: {}'.format(path))
            sleep(.1)
        self.app.modelCamera.setRotation (org_rot)
        self.app.modelCamera.setUp (org_up)

    def random_three_vector(self):
        """
        Generates a random 3D unit vector (direction) with a uniform spherical distribution
        Algo from http://stackoverflow.com/questions/5408276/python-uniform-spherical-distribution
        :return:
        """
        phi = np.random.uniform(0,np.pi*2)
        costheta = np.random.uniform(-1,1)
        theta = np.arccos( costheta )
        x = np.sin( theta) * np.cos( phi )
        y = np.sin( theta) * np.sin( phi )
        z = np.cos( theta )
        return (x,y,z)

    def render(self):
        progress = Progress.begin()
        progress(0, 0.05, "Storing data")
        human = self.app.selectedHuman
        materialBackup = material.Material(human.material)
        progress(0.05, 0.1, "Projecting lightmaps")
        diffuse = imgop.Image(data = human.material.diffuseTexture)
        lmap = projection.mapSceneLighting(
            self.app.scene, border = human.material.sssRScale)
        progress(0.1, 0.4, "Applying medium scattering")
        lmapG = imgop.blurred(lmap, human.material.sssGScale, 13)
        progress(0.4, 0.7, "Applying high scattering")
        lmapR = imgop.blurred(lmap, human.material.sssRScale, 13)
        lmap = imgop.compose([lmapR, lmapG, lmap])
        if not diffuse.isEmpty:
            progress(0.7, 0.8, "Combining textures")
            lmap = imgop.resized(lmap, diffuse.width, diffuse.height, filter=image.FILTER_BILINEAR)
            progress(0.8, 0.9)
            lmap = imgop.multiply(lmap, diffuse)
        lmap.sourcePath = "Internal_Renderer_Lightmap_SSS_Texture"

        progress(0.9, 0.95, "Setting up renderer")
        human.material.diffuseTexture = lmap
        human.configureShading(diffuse = True)
        human.shadeless = True
        progress(0.95, 0.98, None)

        # Render to framebuffer object
        renderprog = Progress()
        renderprog(0, 0.99 - 0.59, "Rendering")
        width, height = 1920, 1080

        log.message("Rendering at %sx%s", width, height)
        """Antialias"""
        width = width * 2
        height = height * 2
        img = mh.renderToBuffer(width, height)
        alphaImg = mh.renderAlphaMask(width, height)
        img = imgop.addAlpha(img, imgop.getChannel(alphaImg, 0))

        renderprog(0.4, 0.99, "AntiAliasing")
        # Resize to 50% using bi-linear filtering
        img = img.resized(width/2, height/2, filter=image.FILTER_BILINEAR)
        # TODO still haven't figured out where components get swapped, but this hack appears to be necessary
        img.data[:,:,:] = img.data[:,:,(2,1,0,3)]
        renderprog.finish()

        human.material = materialBackup
        progress(1, None, 'Rendering complete')
        if DEBUG: self.changeView(img)
        return img

    def changeView(self, img):
        gui3d.app.getCategory('Rendering').getTaskByName('Viewer').setImage(img)
        mh.changeTask('Rendering', 'Viewer')
        gui3d.app.statusPersist('Rendering complete')

class AutoRender(RenderTaskView):
    def __init__(self, category):
        RenderTaskView.__init__(self, category, 'AutoRender')
        G.app.addSetting('GL_RENDERER_SSS', True)
        G.app.addSetting('GL_RENDERER_AA', True)
        # Don't change shader for this RenderTaskView.
        self.outputDest = r'D:\makehuman\mhrender'
        self.taskViewShader = G.app.selectedHuman.material.shader
        box = self.addLeftWidget(gui.GroupBox('Auto Render'))
        box.addWidget(gui.TextView("Ouput Destination"))
        self.destBox = box.addWidget(gui.TextEdit(self.outputDest))
        self.renderButton = box.addWidget(gui.Button('Render'))

        if not mh.hasRenderToRenderbuffer():
            self.firstTimeWarn = True

        @self.renderButton.mhEvent
        def onClicked(event):
            renderThread = RunThread(G.app, self.outputDest)
            renderThread.start()

        @self.destBox.mhEvent
        def onChange(value):
            self.outputDest = value

    def onShow(self, event):
        RenderTaskView.onShow(self, event)
        self.renderButton.setFocus()
        if not mh.hasRenderToRenderbuffer() and self.firstTimeWarn:
            self.firstTimeWarn = False
            G.app.prompt('Lack of 3D hardware support',
                'Your graphics card lacks support for proper rendering.\nOnly limited functionality will be available.',
                'Ok', None, None, None, 'renderingGPUSupportWarning')

    def onHide(self, event):
        RenderTaskView.onHide(self, event)

def load(app):
    category = app.getCategory('Rendering')
    taskview = AutoRender(category)
    taskview.sortOrder = 0.5
    category.addTask(taskview)


def unload(app):
    pass


