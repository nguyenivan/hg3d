import os
import torch.utils.data as data
import cv2
from img import Crop, DrawGaussian, Transform
import matplotlib.pyplot as plt
from matplotlib import animation, rc
from matplotlib.colors import to_rgb
import mpl_toolkits.mplot3d
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import ref
import torch
import math
import re
import h5py as h5
from IPython.display import display, HTML
rc('animation', html='jshtml')


TEST_CASE = False
FAST_LOAD = False
 
class AllTrue(data.Dataset):
  def __init__(self):
    
    self.img_dir = os.path.expanduser('~/hg3d-images/strenxt/')
    self.img_list =  list(filter(lambda x: x.endswith('.png') and (
        not x.startswith('batch1_0')), os.listdir(self.img_dir)))
  
  def LoadImage(self, index):
    path = self.img_dir + self.img_list[index]
    img = cv2.imread(path)
    return img
  
  def __getitem__(self, index):
    img = self.LoadImage(index)
    c = np.ones(2) * ref.strenxtImgSize / 2
    s = ref.strenxtImgSize * 1.0
    r = 0
    try:
      inp = Crop(img, c, s, r, ref.inputRes) / 256.
    except: #MrWin
      print (self.img_dir + self.img_list[index])
      raise

    outp = 1
    inp = torch.from_numpy(inp)
    return inp, outp
  
  def __len__(self):
    return len(self.img_list)

class SomeTrue(data.Dataset):
  def __init__(self):
    if FAST_LOAD:
      self.img_list = ['/content/hg3d-images/strenxt/girl090.png', '/content/hg3d-images/h36m/s_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000031.jpg', '/content/hg3d-images/mpii/000004812.jpg', '/content/hg3d-images/strenxt/random1005.png', '/content/hg3d-images/strenxt/girl088.png', '/content/hg3d-images/strenxt/batch1_0_nguyen05.png', '/content/hg3d-images/strenxt/batch1_0_nguyen01.png', '/content/hg3d-images/mpii/000024100.jpg', '/content/hg3d-images/strenxt/man1091.png', '/content/hg3d-images/strenxt/batch1_1_nguyen07.png', '/content/hg3d-images/strenxt/man1094.png', '/content/hg3d-images/strenxt/Loki078.png', '/content/hg3d-images/h36m/s_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000041.jpg', '/content/hg3d-images/h36m/s_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000016.jpg', '/content/hg3d-images/strenxt/Loki077.png', '/content/hg3d-images/strenxt/batch1_1_nguyen08.png', '/content/hg3d-images/strenxt/Loki071.png', '/content/hg3d-images/strenxt/girl082.png', '/content/hg3d-images/strenxt/man1099.png', '/content/hg3d-images/strenxt/man1093.png', '/content/hg3d-images/strenxt/AnneHathaway054.png', '/content/hg3d-images/strenxt/batch1_0_nguyen06.png', '/content/hg3d-images/strenxt/AnneHathaway052.png', '/content/hg3d-images/strenxt/Loki080.png', '/content/hg3d-images/strenxt/random1003.png', '/content/hg3d-images/strenxt/batch1_1_nguyen09.png', '/content/hg3d-images/h36m/s_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000026.jpg', '/content/hg3d-images/strenxt/man1097.png', '/content/hg3d-images/h36m/s_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000011.jpg', '/content/hg3d-images/mpii/000024293.jpg', '/content/hg3d-images/mpii/000001163.jpg', '/content/hg3d-images/strenxt/girl085.png', '/content/hg3d-images/strenxt/AnneHathaway057.png', '/content/hg3d-images/mpii/000015774.jpg', '/content/hg3d-images/strenxt/man1098.png', '/content/hg3d-images/strenxt/random1008.png', '/content/hg3d-images/strenxt/man1100.png', '/content/hg3d-images/strenxt/random1007.png', '/content/hg3d-images/strenxt/Loki075.png', '/content/hg3d-images/strenxt/girl089.png', '/content/hg3d-images/strenxt/random1006.png', '/content/hg3d-images/strenxt/Loki079.png', '/content/hg3d-images/strenxt/random1004.png', '/content/hg3d-images/mpii/000024087.jpg', '/content/hg3d-images/strenxt/AnneHathaway051.png', '/content/hg3d-images/strenxt/batch1_1_nguyen02.png', '/content/hg3d-images/h36m/s_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000046.jpg', '/content/hg3d-images/mpii/000013469.jpg', '/content/hg3d-images/mpii/000005283.jpg', '/content/hg3d-images/strenxt/girl087.png', '/content/hg3d-images/strenxt/Loki074.png', '/content/hg3d-images/h36m/s_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000036.jpg', '/content/hg3d-images/strenxt/girl086.png', '/content/hg3d-images/strenxt/random1010.png', '/content/hg3d-images/mpii/000022704.jpg', '/content/hg3d-images/strenxt/batch1_0_nguyen04.png', '/content/hg3d-images/mpii/000003072.jpg', '/content/hg3d-images/strenxt/girl081.png', '/content/hg3d-images/h36m/s_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000021.jpg', '/content/hg3d-images/strenxt/Loki072.png', '/content/hg3d-images/h36m/s_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000001.jpg', '/content/hg3d-images/strenxt/girl084.png', '/content/hg3d-images/strenxt/man1092.png', '/content/hg3d-images/strenxt/random1009.png', '/content/hg3d-images/h36m/s_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000006.jpg', '/content/hg3d-images/strenxt/man1096.png', '/content/hg3d-images/strenxt/AnneHathaway056.png', '/content/hg3d-images/strenxt/girl083.png', '/content/hg3d-images/strenxt/man1095.png', '/content/hg3d-images/strenxt/random1001.png', '/content/hg3d-images/strenxt/batch1_1_nguyen03.png', '/content/hg3d-images/strenxt/random1002.png', '/content/hg3d-images/strenxt/Loki073.png', '/content/hg3d-images/strenxt/AnneHathaway058.png', '/content/hg3d-images/strenxt/AnneHathaway055.png', '/content/hg3d-images/strenxt/AnneHathaway053.png', '/content/hg3d-images/strenxt/Loki076.png', '/content/hg3d-images/strenxt/AnneHathaway059.png', '/content/hg3d-images/strenxt/AnneHathaway060.png']
      self.outp_list = [1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    else:
      self.outp_list = [] 
      self.img_dir = os.path.expanduser('~/hg3d-images/strenxt/')
      self.img_list =[os.path.join(self.img_dir, file) for file in list(filter(lambda x: x.endswith('.png') and (
          not x.startswith('batch1_0')), os.listdir(self.img_dir)))]
      self.outp_list = [1] * len(self.img_list)
      _tmp = [os.path.join(self.img_dir, file) for file in list(filter(lambda x: x.endswith('.png') and (
          x.startswith('batch1_0')), os.listdir(self.img_dir)))]
      self.outp_list.extend( [0] * len(_tmp))
      self.img_list.extend(_tmp)

      h36m_list = ['s_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000001.jpg', 
        's_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000006.jpg', 
        's_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000011.jpg', 
        's_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000016.jpg', 
        's_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000021.jpg', 
        's_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000026.jpg', 
        's_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000031.jpg', 
        's_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000036.jpg', 
        's_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000041.jpg', 
        's_01_act_02_subact_01_ca_01/s_01_act_02_subact_01_ca_01_000046.jpg'
      ]

      mpii_list = ['000001163.jpg', 
        '000003072.jpg', 
        '000004812.jpg', 
        '000005283.jpg', 
        '000013469.jpg', 
        '000015774.jpg', 
        '000022704.jpg', 
        '000024087.jpg', 
        '000024100.jpg', 
        '000024293.jpg'
      ]

      self.img_list.extend([os.path.join(ref.h36mImgDir, file) for file in h36m_list])
      self.outp_list.extend([0]*len(h36m_list))
      self.img_list.extend([os.path.join(ref.mpiiImgDir, file) for file in mpii_list])
      self.outp_list.extend([0]*len(mpii_list))
      
      idx = np.random.permutation(len(self.img_list))
      self.img_list = np.array(self.img_list)[idx].tolist()
      self.outp_list = np.array(self.outp_list)[idx].tolist()
#       print self.img_list
#       print self.outp_list
  
  def LoadImage(self, index):
    path = self.img_list[index]
    img = cv2.imread(path)
    return img
  
  def __getitem__(self, index):
    img = self.LoadImage(index)
    c = np.ones(2) * ref.strenxtImgSize / 2
    s = ref.strenxtImgSize * 1.0
    r = 0
    try:
      inp = Crop(img, c, s, r, ref.inputRes) / 256.
    except: #MrWin
      print (self.img_list[index])
      raise

    outp = self.outp_list[index]
    inp = torch.from_numpy(inp)
    return inp, outp
  
  def __len__(self):
    return len(self.img_list)  

###########################  
#Prep 2D
oo = 128
xmax, ymax, zmax = oo, oo, oo
xmin, ymin, zmin = -oo, -oo, -oo
#Prep 3D
max_range = np.array([xmax-xmin, ymax-ymin, zmax-zmin]).max()
Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten() + 0.5*(xmax+xmin)
Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten() + 0.5*(ymax+ymin)
Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten() + 0.5*(zmax+zmin)

def __init__():
  pass

def rotation_matrix(axis, theta_degree):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    """
    theta = math.radians(theta_degree)
    axis = np.asarray(axis)
    axis = axis/math.sqrt(np.dot(axis, axis))
    a = math.cos(theta/2.0)
    b, c, d = -axis*math.sin(theta/2.0)
    aa, bb, cc, dd = a*a, b*b, c*c, d*d
    bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
    return np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
                     [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
                     [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]])

HEAD_SIZE = 6000
TAIL_SIZE = 400
r1 = re.compile('<div class="animation" align="center">.+?(<img id=".+?">).+?</div>', flags = re.M |re.S)
r2 = re.compile(r'document.getElementById\(this.slider_id\).\w+')
r3 = re.compile(r'Animation\.prototype\.get_loop_state = function\(\){.+?return undefined;\s+?}', flags = re.S | re.M)
r4 = re.compile(r'(anim.+?)\s*(=\s*new Animation\(.+?\);)',flags=re.S | re.M)

def anim_html(anim, prediction):
  s = anim.to_jshtml()
  s1 = r1.sub(r'<div class="animation" align="center">\1</div>', s[:HEAD_SIZE])
  s1 = r2.sub('var XXX', s1)
  s1 = r3.sub(r'Animation.prototype.get_loop_state = function(){return "loop";}', s1)
  s2 = r4.sub(r'\1\2\n        \1.play_animation();', s[-TAIL_SIZE:])
  extra_css = '<style>div.animation {display:inline-block; background-color:%s; padding: 8px;} img {display:block;} div.rendered_html {text-align:center;}</style>' % ('green' if prediction else 'red')

  return HTML(extra_css + s1 + s[HEAD_SIZE:-TAIL_SIZE] + s2)

def pose_viz(img, points2d, points3d, prediction, c = 'b'):
  # Setup axes
  crgb = to_rgb(c)
  fig=plt.figure(facecolor='g' if prediction else 'w', edgecolor='r')  
  ax3d = fig.add_subplot((121),projection='3d')
  ax3d.set_xlabel('z') 
  ax3d.set_ylabel('x') 
  ax3d.set_zlabel('y')
  ax3d.set_title('3D Skeleton')
  ax2d = fig.add_subplot((122))
  ax2d.set_title('2D Overlay')
  ax2d.grid(False)
  ax2d.set_xticks([])
  ax2d.set_yticks([])

  fig.suptitle('CORRECT POSE' if prediction else 'WRONG POSE', fontsize=24, fontweight='bold', 
               color='g' if prediction else 'k')
  plt.close()

  # Show 2D
  points = ((points2d.reshape(ref.nJoints, -1))).astype(np.int32)
  for j in range(ref.nJoints):
    cv2.circle(img, (points[j, 0], points[j, 1]), 3, crgb, -1)
  for e in ref.edges:
    cv2.line(img, (points[e[0], 0], points[e[0], 1]),
                  (points[e[1], 0], points[e[1], 1]), crgb, 2)
  ax2d.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))

  for xb, yb, zb in zip(Xb, Yb, Zb):
     ax3d.plot([xb], [yb], [zb], 'w')  

  points = points3d.reshape(ref.nJoints, 3)
  x, y, z = np.zeros((3, ref.nJoints))
  for j in range(ref.nJoints):
    x[j] = points[j, 0] 
    y[j] = - points[j, 1] 
    z[j] = - points[j, 2] 

  points = np.concatenate([[z],[x],[y]], axis=0).T
#   joints_graph = ax3d.scatter(points[:,0], points[:,1], points[:,2], c = c)
  joints_graph, = ax3d.plot(points[:,0], points[:,1], points[:,2], 'o', c = c)

  skeleton_graph = []
  for e in ref.edges:
    bone, = ax3d.plot(points[e][:,0], points[e][:,1], points[e][:,2], c = c)    
    skeleton_graph.append(bone)

  ax3d.view_init(elev=0)

  ROTATE_DEGREE = 18
  ROTATE_AXIS = [0,0,1]
  def animate(i):
    theta_degree = i * ROTATE_DEGREE 
    rot = rotation_matrix(ROTATE_AXIS, theta_degree)
    new_data = np.empty((ref.nJoints, 3))
    for n in xrange(ref.nJoints):
      new_data[n,:] = np.dot(rot, points[n,:]) 
#     for n in xrange(ref.nJoints):    
    joints_graph.set_data(new_data[:,0], new_data[:,1])

    joints_graph.set_3d_properties(new_data[:,2])
    for i, line in enumerate(skeleton_graph):
      e = ref.edges[i]
      line.set_data(new_data[e][:,0], new_data[e][:,1])
      line.set_3d_properties(new_data[e][:,2])
    return skeleton_graph, joints_graph,

  anim = animation.FuncAnimation(fig, animate, frames=20, interval=150, blit=False)
  return anim_html(anim, prediction)

if TEST_CASE:
  with h5.File('/content/hg3d/src/points.h5', 'r') as f:
    img = f['img'][:]
    points2d = f['points2d'][:]
    points3d = f['points3d'][:]
  html = pose_viz(img, points2d, points3d, 1, c = 'b')
  display(html)

    