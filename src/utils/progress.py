from IPython.display import HTML, display
import time

def progress(value, max=100, text = 'Progress'):
    return HTML("""
        <h3>{text}</h3>
        <progress
            value='{value}'
            max='{max}',
            style='width: 90%'
        >
            {value}
        </progress>
    """.format(value=value, max=max, text=text))