#!/usr/bin/env python
import socket
import sys
import struct
import pickle
from cStringIO import StringIO
from io import BytesIO
from PIL import Image
import signal
from time import sleep

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

def cleanup():
    """Close the socket here"""
    print "Clean up...",
    s.close()
    print "OK"

def signal_handler(signal, frame):
    cleanup()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

def dowork(HOST, PORT):
    try:
        s.settimeout(1)
        print "Connecting to {}:{}".format(HOST,PORT)
        s.connect((HOST, PORT))
    except socket.error as err:
        print 'Connect failed, error code: ' + str(err)
        cleanup()
        sys.exit()
    print "Connected, done!"
    cleanup()

import sys
if __name__=="__main__":
    try:
        HOST =  sys.argv[1]
        PORT =  int(sys.argv[2])
        if len(sys.argv) > 2:
            dowork(HOST, PORT)
            exit(0)
    except Exception as e:
        print e
    print "Usage: client [HOST] [PORT]"