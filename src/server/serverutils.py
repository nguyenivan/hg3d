from __future__ import division
import os
import matplotlib
if os.environ.get('DISPLAY','') == '':
    print('No display found. Using non-interactive Agg backend')
    matplotlib.use('Agg')

from matplotlib import pyplot as plt
import cvlib as cv
from cvlib.object_detection import draw_bbox
import sys
import cv2
import numpy as np


class MovingAverageFilter:
  """Calculate average for input signal"""
  def __init__(self, N=3):
    self.N = N
    """Number of Lookback"""
    self.__cumsum = [0]
    
  def process(self, value):
    """
    return: None if too early (t<N)
    """
    if len(self.__cumsum) == 1:
      self.__cumsum.append(value)
      return None
    
    current_cumsum = self.__cumsum[-1] + value
    
    moving_average = None
    if len(self.__cumsum) >= self.N:
        moving_average = (current_cumsum - self.__cumsum[-(self.N)])/self.N
        self.__cumsum.pop(0)            
    self.__cumsum.append(current_cumsum)
    return moving_average
  
  def __test(self):
    a= [40, 30, 50, 46, 39, 44]
    b=[]
    for i in a:
      b=b+[self.process(i)]
    assert b== [40.0, 42.0, 45.0, 43.0]

def detect_human(image):
  # Courtesy https://github.com/arunponnusamy/cvlib/tree/master/examples
  # apply object detection. 'conf' stands for confidence
  bbox, label, conf = cv.detect_common_objects(image)
  # ([[1028.0, 85.0, 1361.0, 1006.0], [1750.0, 554.0, 1916.0, 881.0]], ['person', 'chair'], [0.9960377216339111, 0.7573862075805664]
  bbox = np.array(bbox).astype(int).tolist()
  if 'person' in label:
    bbox_list = np.array(bbox)[np.array(label) == 'person'].tolist()
  else:
    bbox_list = []
  return bbox_list  
 
def square_crop(image):
  """
    Crop an image to a square that contain human. Default to center if no human detected
  """
  height, width, channels = image.shape
  bbox_list = detect_human(image)
  if bbox_list:
    bbox = bbox_list[0]
    x1,y1,x2,y2 = bbox
    bbw, bbh = x2-x1, y2-y1
    pad_left = pad_right = pad_top = pad_bottom = 0
    if bbh > bbw:
      #Extend width
      center = (x2+x1)//2
      x1,x2 = center - bbh//2, center + (bbh - bbh//2)
      if x2>width:
        pad_right = x2 - width
        x2 = width
      if x1<0:
        pad_left = 0 - x1
        x1 = 0
#       print (x1,y1,x2,y2)
#       print (width, height)
    else:
      # Extend height
      center = (y2+y1)//2
      y1,y2 = center - bbw//2, center + (bbw - bbw//2)
      if y2>height:
        pad_bottom = y2 - height
        y2 = height
      if y1<0:
        pad_top = 0 - y1
        y1 = 0
    cropped = image[y1:y2, x1:x2]
    if pad_top or pad_bottom or pad_left or pad_right:
      cropped = np.pad(cropped, ( (pad_top, pad_bottom), (pad_left, pad_right), (0,0) ), mode='constant', constant_values=0)
    return cropped
  else:
    if width > height:
      delta = height // 2
      half_width = width //2
      x1,y1,x2,y2 = (half_width - delta, 0, half_width + delta, height)
    else:
      delta = width // 2
      half_height= height //2
      x1,y1,x2,y2 = (0, half_height - delta, width, half_height + delta)
      cropped = image[y1:y2, x1:x2]
      return cropped

def test():
    """Unit test"""
    file_path = '/content/hg3d/images/6.jpg'
    image = cv2.imread(file_path)
    cropped = square_crop(image)
    cv2.imwrite('output.jpg', cropped)    
    #plt.imshow(cropped)
