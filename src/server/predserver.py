#!/usr/bin/env python
import os,sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

import socket
import sys
import struct
import pickle
from cStringIO import StringIO
from io import BytesIO
from PIL import Image
import signal
from time import sleep
import cv2
import numpy as np
from serverutils import square_crop
from utils.eval import SimplePreds
from termcolor import colored
import torch
import ref

MAX_SOCK_TOLERANCE = 5

s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)

def cleanup():
    # close the socket here
    print "Clean up...",
    s.close()
    print "OK"


def signal_handler(signal, frame):
    cleanup()
    sys.exit(0)


model = torch.load('/content/hg3d/exp/look_phone/posenet_1.pth', strict=False).cuda()

signal.signal(signal.SIGINT, signal_handler)


HOST = '' #this is your localhost
PORT = 8080
i = 0
while True:
    try:
        s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    #Bind socket to Host and Port
        s.bind((HOST, PORT))
    except socket.error as err:
        print 'Bind Failed, Error Code: ' + str(err[0]) + ', Message: ' + err[1]
        s.close()
        sys.exit()

    print 'Socket Bind Success!'
    threshold = 4
    data =b""
    try:
        #Sets up and start TCP listener.
        s.listen(10)
        print 'Socket is now listening ({})'.format(i)
        s.settimeout(3)
        while True:
            try:
                conn,addr=s.accept()
            except socket.timeout:
                sleep(.1)
                continue
            break
        print "Connected"
        conn.settimeout(1)
        print "Sending RACK...",
        attempt = 0
        while True:
            try:
                conn.sendall(b"RACK")
                attempt = 0
            except socket.timeout, e:
                if attempt < MAX_SOCK_TOLERANCE:
                    attempt += 1
                    print "Nobody listen, resending"
                    sleep(.1)
                    continue
                else:
                    raise e

            print "OK"  
            break

        length_size = struct.calcsize(">L")

        data = b""
        attempt = 0
        while True:
            try:
                recv = conn.recv(1024)
                attempt = 0
            except socket.timeout, e:
                if attempt < MAX_SOCK_TOLERANCE:
                    attempt +=  1
                    print ".",
                    sys.stdout.flush()
                    sleep(.1)
                    continue
                else:
                    raise e

            if len(recv) == 0:
                #Client disconnected
                raise socket.error
            data += recv
            if len(data) >= length_size:
                length_packed = data[:length_size]
                data = data[length_size:]
                break
            else:
                #Only read < 4 bytes so far, need to read more 
                continue

        data_length = struct.unpack(">L", length_packed)
        data_length = data_length[0]
        print "Data length:", data_length, "bytes"
        attempt = 0
        while len(data)< data_length:
            try:
                print ".",
                sys.stdout.flush()
                recv = conn.recv(4096)
                attempt = 0
            except socket.timeout, e:
                if attempt < MAX_SOCK_TOLERANCE:
                    attempt += 1
                    sleep(.1)
                    attempt += 1
                    continue
                else:
                    raise e
            if len(recv) == 0:
                #Client disconnected
                raise socket.error
            data += recv
        
	print "Received", len(data), "bytes"
        assert len(data) == data_length, "Data length is {}, expted {}. Please check client.".format(len(data),
        data_length)

        byte_data = np.asarray(bytearray(data)) 
        image = cv2.imdecode(byte_data, cv2.IMREAD_COLOR)
        dim = (256,256)
        if image.shape[:2] != dim:
            print ("Resizing from {} to {}".format(image.shape, dim))
            cropped = square_crop(image)
            image = cv2.resize(cropped, dim, interpolation = cv2.INTER_AREA) 
        input = torch.from_numpy(image.transpose(2, 0, 1)).float() / 256.
        input = input.view(1, input.size(0), input.size(1), input.size(2))
        input_var = torch.autograd.Variable(input).float().cuda()
        _,pred, reg, output = model(input_var)
        prediction = SimplePreds(output.data.cpu().numpy())
        print ("{} {}".format(output.data.cpu().numpy(), prediction))
        print (colored("Prediction --> {}".format(prediction), 'green' if prediction else 'red'))
        
        # Send ACK + pred and we are done
        attempt = 0
        while True:
            try:
                conn.sendall(b"ACK" + str(prediction[0]))
            except socket.timeout:
                if attempt < MAX_SOCK_TOLERANCE:
                    attempt += 1
                    sleep(.1)
                    continue
                else:
                    raise socket.timeout
            break
        file_name = "{}_{}.png".format(i, bool(prediction))
        print ("Saving to {}".format(file_name))
        cv2.imwrite(file_name, image)
        print ("Done({})!".format(i))
        sys.stdout.flush()

            
    except Exception as e:
        #if there is any exception the client will wait for ACK1 forever TODO
	print str(e)

    cleanup()

    # print ('{} --> {}'.format(file_path, prediction))
    i += 1

  
