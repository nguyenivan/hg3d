#!/usr/bin/env python
import os,sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
import matplotlib
if os.environ.get('DISPLAY','') == '' or matplotlib.get_backend().lower()!="agg":
    print('No display found. Using non-interactive Agg backend')
    matplotlib.use('Agg')

from utils.eval import SimplePreds
import torch
import cv2
from serverutils import square_crop
from matplotlib import pyplot as plt
from termcolor import colored



img_dir = '/content/hg3d/src/test'
file_list = list(filter(lambda x: x.endswith('.png') or x.endswith('.jpg'), os.listdir(img_dir)))
model = torch.load('/content/hg3d-images/exp/look_phone/posenet_4.pth').cuda()
for file_name in file_list:
  file_path = os.path.join(img_dir, file_name)
  image = cv2.imread(file_path)
  dim = (256,256)
  cropped = square_crop(image)
  #plt.imshow(cropped)
  #plt.show()
  img = cv2.resize(cropped, dim, interpolation = cv2.INTER_AREA) 
  input = torch.from_numpy(img.transpose(2, 0, 1)).float() / 256.
  input = input.view(1, input.size(0), input.size(1), input.size(2))
  input_var = torch.autograd.Variable(input).float().cuda()
  _,pred, reg, output = model(input_var)
  prediction = SimplePreds(output.data.cpu().numpy())
  print colored( '{} --> {}'.format(file_path, prediction), 'green' if prediction else 'white')
