import socket
import sys
import struct
import pickle
from cStringIO import StringIO
from io import BytesIO
from PIL import Image
import signal
from time import sleep

s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)

def cleanup():
    """Close the socket here"""
    print "Clean up...",
    s.close()
    print "OK"


def signal_handler(signal, frame):
    cleanup()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

HOST = '' #this is your localhost
def dowork(PORT):
    try:
        s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        #Bind socket to Host and Port
        s.bind((HOST, PORT))
    except socket.error as err:
        print 'Bind Failed, Error Code: ' + str(err[0]) + ', Message: ' + err[1]
        s.close()
        sys.exit()
    print 'Socket Bind Success!'
    try:
        #Sets up and start TCP listener.
        s.listen(10)
        print 'Socket is now listening'
        s.settimeout(3)
        while True:
            try:
                conn,addr=s.accept()
            except socket.timeout:
                sleep(.5)
                continue
            break
        print "Connected, done!"
                
    except Exception as e:
        print str(e)

    cleanup()

import sys
if __name__=="__main__":
    try:
        PORT =  int(sys.argv[1])
        if len(sys.argv) > 1 and PORT:
            dowork( PORT)
            exit(0)
    except:
        exit(0)
    print "Usage: tport [PORT]"