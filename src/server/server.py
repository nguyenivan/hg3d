import socket
import sys
import struct
import pickle
from cStringIO import StringIO
from io import BytesIO
from PIL import Image
import signal
from time import sleep

s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)

def cleanup():
    # close the socket here
    print "Clean up...",
    s.close()
    print "OK"


def signal_handler(signal, frame):
    cleanup()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

HOST = '' #this is your localhost
PORT = 8080
i = 0
while True:
    try:
        s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    #Bind socket to Host and Port
        s.bind((HOST, PORT))
    except socket.error as err:
        print 'Bind Failed, Error Code: ' + str(err[0]) + ', Message: ' + err[1]
        s.close()
        sys.exit()

    print 'Socket Bind Success!'
    threshold = 4
    data =b""
    try:
        #Sets up and start TCP listener.
        s.listen(10)
        print 'Socket is now listening ({})'.format(i)
        s.settimeout(3)
        while True:
            try:
                conn,addr=s.accept()
            except socket.timeout:
                sleep(.5)
                continue
            break
        print "Connected"
        conn.settimeout(1)
        print "Sending RACK...",
        while True:
            try:
                conn.sendall(b"RACK")
            except socket.timeout:
                print "Nobody listen resending"
                sleep(1)
                continue
            print "OK"  
            break

        length_size = struct.calcsize(">L")
        print "Length size:", length_size

        data = b""
        while True:
            try:
                recv = conn.recv(1024)
            except socket.timeout, e:
                print ".",
                sys.stdout.flush()
                sleep(.5)
                continue
            if len(recv) == 0:
                #Client disconnected
                raise socket.error
            data += recv
            if len(data) >= length_size:
                length_packed = data[:length_size]
                print "Length packed:", length_packed
                data = data[length_size:]
                break
            else:
                #Only read < 4 bytes so far, need to read more 
                continue

        data_length = struct.unpack(">L", length_packed)
        data_length = data_length[0]
        print "Data length:", data_length, "bytes"
        print "Carried over ", len(data), "bytes"
        print "Start receiving"

        while len(data)< data_length:
            try:
                recv = conn.recv(4096)
            except socket.timeout:
                print ".",
                sys.stdout.flush()
                sleep(.5)
                continue
            if len(recv) == 0:
                #Client disconnected
                print "Socket error"
                break
            data += recv
            print "Received", len(data), "bytes"
        
        assert len(data) == data_length, "Data length is {}, expted {}. Please check client.".format(len(data),
        data_length)
        
        # Send FACK and we are done
        print "Sending FACK...",
        while True:
            try:
                conn.sendall(b"FACK")
            except socket.timeout:
                print "Nobody listen resending"
                sleep(1)
                continue
            print "OK"
            break
        
        print ("Done({})!".format(i))
            
    except Exception as e:
        print str(e)

    print "Connection end."
    cleanup()
    file_data = BytesIO(data)
    image = Image.open(file_data)
    image.save('output{}.png'.format(i))
    i += 1