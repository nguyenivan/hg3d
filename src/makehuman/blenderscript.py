"""
Block1
"""
import bpy
file_path = r"D:\makehuman\test\withbg.mhx2"

for i in range(200):
    print("Process {}".format(i), end ="", flush=True)
    mhx2 = bpy.ops.import_scene.makehuman_mhx2(filepath=file_path)
    for i in bpy.data.materials:
        if i.name.startswith('Withbg'):
            bpy.data.materials.remove(i)
    for i in bpy.data.objects:
        if i.name.startswith('Withbg'):
            bpy.data.objects.remove(i)
    print ("Done!")
    	
"""
Rotate Camera
"""
import bpy
import mathutils

def update_camera(camera, focus_point=mathutils.Vector((0.0, 0.0, 0.0)), distance=10.0):
    """
    Focus the camera to a focus point and place the camera at a specific distance from that
    focus point. The camera stays in a direct line with the focus point.

    :param camera: the camera object
    :type camera: bpy.types.object
    :param focus_point: the point to focus on (default=``mathutils.Vector((0.0, 0.0, 0.0))``)
    :type focus_point: mathutils.Vector
    :param distance: the distance to keep to the focus point (default=``10.0``)
    :type distance: float
    """
    looking_direction = camera.location - focus_point
    rot_quat = looking_direction.to_track_quat('Z', 'Y')

    camera.rotation_euler = rot_quat.to_euler()
    camera.location = rot_quat * mathutils.Vector((0.0, 0.0, distance))

update_camera(bpy.data.objects['Camera'])

"""
Block2
"""
import bpy
import random
from time import sleep
import numpy as np
from mathutils import Vector
import sys
from bpy.app.handlers import persistent



class RenderOp(bpy.types.Operator):
    """Operator which runs its self from a timer"""
    bl_idname = "wm.render_op"
    bl_label = "Modal Timer Operator"

    limits = bpy.props.IntProperty(default=0)
    _timer = None

    orig_loc = None
    orig_rot = None
    running = False
    counter = 0

    def clean_up(self):
        camera = bpy.data.objects["Camera"]
        if self.orig_loc is not None:
            camera.location = self.orig_loc
        if self.orig_rot is not None:
            camera.rotation_euler = self.orig_rot
        print ("DONE!")

    def modal(self, context, event):
        if event.type in {'RIGHTMOUSE', 'ESC'} or self.limits > 5:
            self.limits = 0
            self.cancel(context)
            self.clean_up()
            return {'FINISHED'}

        if not self.running and event.type == 'TIMER':
            self.running = True
            self.counter += 1
            print (str(self.counter), end=" ", flush=True)
            self.random_on_sphere()
            sleep(.2)            

        return {'PASS_THROUGH'}
    
        
    def draw_callback_view(self, caller, context):
        self.running = False

    def execute(self, context):
        self.counter = 0
        wm = context.window_manager
        self._handle = bpy.types.SpaceView3D.draw_handler_add(self.draw_callback_view, (self, context), 'WINDOW', 'POST_VIEW')
        self._timer = wm.event_timer_add(time_step=10, window=context.window)
        wm.modal_handler_add(self)
        
        return {'RUNNING_MODAL'}

    def cancel(self, context):
        wm = context.window_manager
        wm.event_timer_remove(self._timer)
        bpy.types.SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')

    def random_on_sphere(self):
        # Test
        camera = bpy.data.objects["Camera"]
        center = Vector( (0,np.random.uniform(.25,.75),0) )
        distance = np.linalg.norm(camera.location - center )
        new_vec = Vector(self.random_three_vector() * distance)
        new_loc = new_vec + center
        camera.location = new_loc
        self.look_at(camera, center)

    def random_three_vector(self):
        """
        Generates a random 3D unit vector (direction) with a uniform spherical distribution
        Algo from http://stackoverflow.com/questions/5408276/python-uniform-spherical-distribution
        :return:
        """
        phi = np.random.uniform(-.75*np.pi,-.25*np.pi)
        costheta = np.random.uniform(np.cos(np.pi/6), np.cos(np.pi/3))
        theta = np.arccos( costheta )
        x = np.sin( theta) * np.cos( phi )
        y = np.sin( theta) * np.sin( phi )
        z = np.cos( theta )
        return np.array([x,y,z])

    def look_at(self, obj_camera, point):
        loc_camera = obj_camera.matrix_world.to_translation()
        direction = point - loc_camera
        # point the cameras '-Z' and use its 'Y' as up
        rot_quat = direction.to_track_quat('-Z', 'Y')
        # assume we're using euler rotation
        obj_camera.rotation_euler = rot_quat.to_euler()

@persistent
def scene_render_pre_handler( dummy ):    # <--- yes, really type dummy here
    print("Prerender")



def register():
 
    bpy.app.handlers.render_post.append(scene_render_pre_handler)
    bpy.utils.register_class(RenderOp)

def unregister():
    bpy.utils.unregister_class(RenderOp)


if __name__ == "__main__":
    register()
    camera = bpy.data.objects["Camera"]
    if RenderOp.orig_loc is None:
        RenderOp.orig_loc = camera.location
    if RenderOp.orig_rot is None:
        RenderOp.orig_rot = camera.rotation_euler
    
    bpy.ops.wm.render_op()



"""
Block3: Adding curves
"""
import bpy
from mathutils import Vector

w = 1 # weight
cList = [Vector((0,0,0)),Vector((1,0,0)),Vector((2,0,0)),Vector((2,3,0)),
        Vector((0,2,1))]

curvedata = bpy.data.curves.new(name='Curve', type='CURVE')
curvedata.dimensions = '3D'

objectdata = bpy.data.objects.new("ObjCurve", curvedata)
objectdata.location = (0,0,0) #object origin
bpy.context.scene.objects.link(objectdata)

polyline = curvedata.splines.new('POLY')
polyline.points.add(len(cList)-1)
for num in range(len(cList)):
    x, y, z = cList[num]
    polyline.points[num].co = (x, y, z, w)

or a slightly modified version, for turning it into a reusable function
import bpy
from mathutils import Vector

w = 1 # weight
listOfVectors = [Vector((0,0,0)),Vector((1,0,0)),Vector((2,0,0)),Vector((2,3,0)),
        Vector((0,2,1))]

def MakePolyLine(objname, curvename, cList):
    curvedata = bpy.data.curves.new(name=curvename, type='CURVE')
    curvedata.dimensions = '3D'

    objectdata = bpy.data.objects.new(objname, curvedata)
    objectdata.location = (0,0,0) #object origin
    bpy.context.scene.objects.link(objectdata)

    polyline = curvedata.splines.new('POLY')
    polyline.points.add(len(cList)-1)
    for num in range(len(cList)):
        x, y, z = cList[num]
        polyline.points[num].co = (x, y, z, w)

MakePolyLine("NameOfMyCurveObject", "NameOfMyCurve", listOfVectors)

If the list of vectors has only one Vector, then the curve will use one predefined zero vector (Vector((0,0,0))) and draw a straight line from 0,0,0 to your single Vector. Go into edit mode to see where the points are placed.

Each object can have multiple curves associated with it, they are accessed like this
# returns the number of curves associated with this object
>>> len(bpy.context.active_object.data.splines)

# returns the curve information associated with the first spline (index = 0) 
>>> bpy.context.active_object.data.splines[0]
bpy.data.curves["NameOfMyCurve"].splines[0]

# this returns the coordinates on that curve as a list.
>>> [point.co for point in bpy.context.active_object.data.splines[0].points]
if you want a smooth curve from these points, declare at the time of creation what type from these options ('POLY', 'BEZIER', 'BSPLINE', 'CARDINAL', 'NURBS')
import bpy  
from mathutils import Vector  
  
w = 1 # weight  
listOfVectors = [Vector((0,0,0)),Vector((1,0,0)),Vector((2,0,0)),Vector((2,3,0)),  
        Vector((0,2,1))]  
  
def MakePolyLine(objname, curvename, cList):  
    curvedata = bpy.data.curves.new(name=curvename, type='CURVE')  
    curvedata.dimensions = '3D'  
  
    objectdata = bpy.data.objects.new(objname, curvedata)  
    objectdata.location = (0,0,0) #object origin  
    bpy.context.scene.objects.link(objectdata)  
  
    polyline = curvedata.splines.new('NURBS')  
    polyline.points.add(len(cList)-1)  
    for num in range(len(cList)):  
        x, y, z = cList[num]  
        polyline.points[num].co = (x, y, z, w)  
  
    polyline.order_u = len(polyline.points)-1
    polyline.use_endpoint_u = True
    
  
MakePolyLine("NameOfMyCurveObject", "NameOfMyCurve", listOfVectors)
And here a more stripped down version
import bpy  
from mathutils import Vector  

# weight  
w = 1 

# we don't have to use the Vector() notation.  
listOfVectors = [(0,0,0),(1,0,0),(2,0,0),(2,3,0),(0,2,1)]  
  
def MakePolyLine(objname, curvename, cList):  
    curvedata = bpy.data.curves.new(name=curvename, type='CURVE')  
    curvedata.dimensions = '3D'  
  
    objectdata = bpy.data.objects.new(objname, curvedata)  
    objectdata.location = (0,0,0) #object origin  
    bpy.context.scene.objects.link(objectdata)  
  
    polyline = curvedata.splines.new('NURBS')  
    polyline.points.add(len(cList)-1)  
    for num in range(len(cList)):  
        polyline.points[num].co = (cList[num])+(w,)  
  
    polyline.order_u = len(polyline.points)-1
    polyline.use_endpoint_u = True
    
  
MakePolyLine("NameOfMyCurveObject", "NameOfMyCurve", listOfVectors)


"""
Block4: Camera direction (not 100 works)
"""
mat= camob.matrixWorld
dir= Mathutils.Vector(0,0,1,1)*mat
dir.resize3D()
dir.normalize() # optonal
# dir is the camera direction

"""
Block5: Zoom Cam to BBox
"""
import bpy
from mathutils import Vector
#for obj in bpy.context.selected_objects
import numpy as np
def global_bbox_center(o):
    local_bbox_center = 0.125 * sum((Vector(b) for b in o.bound_box), Vector())
    ret = o.matrix_world * local_bbox_center
    return ret 

min_dim = np.array((9999,9999,9999))
max_dim = np.array((-9999,-9999,-9999))
for bpart in bpy.data.objects:
    if bpart.name.startswith('Render'):
        min_bbox = np.array(global_bbox_center(bpart) - (bpart.dimensions / 2))
        max_bbox = np.array(global_bbox_center(bpart) + (bpart.dimensions / 2))
        min_dim = np.min( (min_dim, min_bbox), 0 )
        max_dim = np.max( (max_dim, max_bbox), 0 )

cube = bpy.data.objects['Cube']
cube.location = Vector( (min_dim + max_dim) / 2 )
cube.dimensions = Vector( (max_dim - min_dim) ) * 1.05

bpy.ops.object.select_all(action='DESELECT')
cube.select = True

bpy.ops.view3d.camera_to_view_selected()


