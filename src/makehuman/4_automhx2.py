#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
try:
    from exportutils.config import Config as ExportConfig
except ImportError:
    from export import ExportConfig

class Mhx2Config(ExportConfig):
    def __init__(self):
        ExportConfig.__init__(self)
        self.useBinary     = False

import gui3d
import mh
import gui
import log
import os
from core import G
from progress import Progress
import numpy as np
from time import sleep
from threading import Thread, Event, Condition
from getpath import getPath

from . import mh2mhx2

DEBUG = False
REPEAT = 4   
HR = [0,15]
VR = [-85,85]
RANDOM_UP = False
FILE_BASE = 'render'
FILE_EXT = 'jpg'
MODEL_FINE_TUNE = 11
WMIN = 0 # in percentage
WMAX = 1 # in percentage
WRANGE = np.linspace(WMIN, WMAX, num=MODEL_FINE_TUNE, dtype = float)
HMIN = 0
HMAX = 1
HRANGE = np.linspace(HMIN, HMAX, num=MODEL_FINE_TUNE, dtype = float)

class ExportThread(Thread):
    def __init__(self, e, task):
        Thread.__init__(self)
        self.human = G.app.selectedHuman
        self.task = task
        self.stop = False
        self.e = e

    def progress(self, progress, status=None):
        pass

    def run(self):
        org_w = self.human.getWeight()
        org_h = self.human.getHeight()
        log.message("Begin Exporting")
        for w in WRANGE:
            if self.stop:
                break
            mh.callAsyncThread(G.app.selectedHuman.setWeight,w)
            for h in HRANGE:
                if self.stop:
                    break
                mh.callAsyncThread(G.app.selectedHuman.setHeight,h)
                mh.redraw()
                sleep(.5)
                log.message("Exporting human weight:%s, height:%s", w, h)
                tag = "w{}_h{}".format(w,h)
                mh.callAsyncThread(self.task, tag)
                self.e.wait()
                self.e.clear()
        self.human.setWeight(org_w)
        self.human.setHeight(org_h)

class AutoMhx2(gui3d.TaskView):
    def __init__(self, category):
        gui3d.TaskView.__init__(self, category, 'AutoMhx2')
        G.app.addSetting('GL_RENDERER_SSS', True)
        G.app.addSetting('GL_RENDERER_AA', True)
        self.outputDest = getPath('data/render')
        box = self.addLeftWidget(gui.GroupBox('Auto Export Mhx2'))
        box.addWidget(gui.TextView("Ouput Destination"))
        self.destBox = box.addWidget(gui.TextEdit(self.outputDest))
        self.renderButton = box.addWidget(gui.Button('Export Mhx2'))
        self.stopButton = box.addWidget(gui.Button('Stop Export'))
        self.e = Event()
        self.human = G.app.selectedHuman
        self.cfg = Mhx2Config()
        self.cfg.useTPose          = False
        self.cfg.useBinary         = True
        self.cfg.useExpressions    = True
        self.cfg.usePoses          = True
        self.cfg.feetOnGround      = True
        self.cfg.scale,self.cfg.unit    = (1,'decimeter')
        self.cfg.setHuman(self.human)

        self.renderThread = None

        @self.renderButton.mhEvent
        def onClicked(event):
            if self.renderThread is None:
                if not os.path.exists(self.outputDest):
                    os.makedirs(self.outputDest)
                self.renderThread = ExportThread(self.e, self.export)
                self.renderThread.start()

        @self.stopButton.mhEvent
        def onClicked(event):
            if self.renderThread is not None:
                self.renderThread.stop = True
                self.renderThread = None

        @self.destBox.mhEvent
        def onChange(value):
            self.outputDest = value

    def export(self, tag=""):
        path = os.path.join(self.outputDest, 
            '{}{}.{}'.format(FILE_BASE, tag, "mhx2") )
        mh2mhx2.exportMhx2(path, self.cfg)
        self.e.set()
       

    def onShow(self, event):
        gui3d.TaskView.onShow(self, event)
        self.renderButton.setFocus()

    def onHide(self, event):
        gui3d.TaskView.onHide(self, event)

def load(app):
    category = app.getCategory('Rendering')
    taskview = AutoMhx2(category)
    taskview.sortOrder = 1.5
    category.addTask(taskview)

def unload(app):
    pass



