"""
Block2
"""
import bpy
import random
from time import sleep
import numpy as np
from mathutils import Vector
import sys
from bpy.app.handlers import persistent
from datetime import datetime
from bpy.props import StringProperty, IntProperty
import os


class RenderOp(bpy.types.Operator):
    """Operator which runs its self from a timer"""
    bl_idname = "wm.render_op"
    bl_label = "Modal Timer Operator"

    _timer = None

    orig_loc = None
    orig_rot = None
    
    counter = IntProperty(default = 0)
    _debug = True
    last_run = datetime.now()
    render_path = StringProperty(default="")
    import_path = StringProperty(default="")
    file_prefix = StringProperty(default="")
    MAX_RENDER = 2

    def __init__(self):
        self.mhx2_files = enumerate(())
        self.running = False
        self.rendering = False
        self.last_import_prefix = "sdgsgkjhkKKH325hkhk"
        self.cube = bpy.data.objects['Cube']
        

    def __del__(self):
        camera = bpy.data.objects["Camera"]
        if self.orig_loc is not None:
            camera.location = self.orig_loc
        if self.orig_rot is not None:
            camera.rotation_euler = self.orig_rot
        if self._debug:
            print (self.orig_loc, self.orig_rot)
        self.render_path = ""
        self.file_prefix = ""
        if self._debug:
            print ("RenderOp End")

    def modal(self, context, event):
        if event.type in {'RIGHTMOUSE', 'ESC'}:
            self.counter = 0
            self.cancel(context)
            return {'FINISHED'}

        time_gap = (datetime.now() - self.last_run).total_seconds()
        if event.type == 'TIMER' and not self.running and not self.rendering and time_gap > 2.0:
            self.last_run = datetime.now()
            self.running = True
            if self.counter % self.MAX_RENDER == 0:
                if self._debug:
                    print("Loading: {}".format(self.counter // self.MAX_RENDER))
                try:
                    (file_index, file_name) = next(self.mhx2_files)
                except StopIteration:
                    return {'FINISHED'}
                if self._debug:
                    print("File to load:{} {}".format(file_index, os.path.join(self.import_path, file_name)))
                self.clear_and_import(os.path.join(self.import_path, file_name))
                self.last_import_prefix, _ = os.path.splitext(file_name)
                self.last_import_prefix = self.last_import_prefix.lower()

            if self._debug:
                print("Rendering: {}".format(self.counter))
            self.rendering = True
            phi, theta = self.random_on_sphere()
            file_name = "{}_{}_{}_{}.jpg".format(self.file_prefix, self.counter, phi, theta)
            file_name = os.path.join(self.render_path, file_name)
            bpy.data.scenes['Scene'].render.filepath = file_name
            bpy.ops.render.render(write_still=True )
            self.counter += 1
            self.running = False

        return {'PASS_THROUGH'}
    
    def clear_and_import(self, file_path):
        if self._debug:
            print ("Removing {}*".format(self.last_import_prefix))
        for i in bpy.data.materials:
            if i.name.lower().startswith(self.last_import_prefix):
                bpy.data.materials.remove(i)
        for o in bpy.data.objects:
            if not o.hide_select and not o.hide_render and not o.hide:
                    bpy.data.objects.remove(o)
        ret = bpy.ops.import_scene.makehuman_mhx2(filepath=file_path)
        return ret

    def draw_callback_view(self, caller, context):
        self.rendering = False

    def execute(self, context):
        if self._debug:
            print ("EXECUTE")
        if self.render_path == "":
            raise ValueError("Must set render_path when call RenderOp operator")
        if not os.path.exists(self.render_path):
            os.mkdir(self.render_path)

        if self.import_path == "":
            raise ValueError("Must set import_path when call RenderOp operator")

        self.mhx2_files =enumerate(filter(lambda x: x.endswith('.mhx2'), os.listdir(self.import_path)))
        # if not self.mhx2_files:
        #     print ("There is MHX2 in this folder: {}".format(import_path))
        #     return {'FINISHED'}

        self.counter = 0
        wm = context.window_manager
        self._handle = bpy.types.SpaceView3D.draw_handler_add(self.draw_callback_view, (self, context), 'WINDOW', 'POST_VIEW')
        self._timer = wm.event_timer_add(time_step=1, window=context.window)
        wm.modal_handler_add(self)
        
        return {'RUNNING_MODAL'}

    def cancel(self, context):
        wm = context.window_manager
        wm.event_timer_remove(self._timer)
        bpy.types.SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')

    def random_on_sphere(self):
        # Test
        camera = bpy.data.objects["Camera"]
        center = Vector( (0,0,np.random.uniform(0.2,1.5)) )
        distance = 2.5
        random_unit, phi, theta = self.random_three_vector() 
        new_vec = Vector(random_unit * distance)
        new_loc = new_vec + center
        camera.location = new_loc
        self.look_at(camera, center)
        self.camera_bound()
        if self._debug:
            print ("Center: {}".format(str(center)))
            print ("Camera: {}".format(str(new_loc)))
            print ("Distance: {}".format(str(distance)))
        return phi, theta

    def random_three_vector(self):
        """
        Generates a random 3D unit vector (direction) with a uniform spherical distribution
        Algo from http://stackoverflow.com/questions/5408276/python-uniform-spherical-distribution
        :return:
        """
        phi = np.random.uniform(-.75*np.pi,-.25*np.pi)
        costheta = np.random.uniform(np.cos(np.pi/3.0), np.cos(np.pi/2.0))
        theta = np.arccos( costheta )
        x = np.sin( theta) * np.cos( phi )
        y = np.sin( theta) * np.sin( phi )
        z = np.cos( theta )
        return np.array([x,y,z]), phi, theta

    def look_at(self, camera, point):
        # camera_loc = camera.matrix_world.to_translation()
        camera_loc = camera.location
        direction = camera_loc - point
        # point the cameras 'Z' and use its 'Y' as up
        rot_quat = direction.to_track_quat('Z', 'Y')
        # assume we're using euler rotation
        camera.rotation_euler = rot_quat.to_euler()

    def global_bbox_center(self, o):
        local_bbox_center = 0.125 * sum((Vector(b) for b in o.bound_box), Vector())
        ret = o.matrix_world * local_bbox_center
        return ret 

    def camera_bound(self):
        min_dim = np.array((9999,9999,9999))
        max_dim = np.array((-9999,-9999,-9999))
        for bpart in bpy.data.objects:
            if not bpart.hide_select and not bpart.hide_render and not bpart.hide:
                min_bbox = np.array(self.global_bbox_center(bpart) - (bpart.dimensions / 2))
                max_bbox = np.array(self.global_bbox_center(bpart) + (bpart.dimensions / 2))
                min_dim = np.min( (min_dim, min_bbox), 0 )
                max_dim = np.max( (max_dim, max_bbox), 0 )
        if np.linalg.norm(min_dim) > 9999:
            self.cube.location = Vector( (0.0,0.0,1.0) )
            self.cube.dimensions = Vector( (1.0, 2.0, 1.0) )
        else:
            self.cube.location = Vector( (min_dim + max_dim) / 2 )
            self.cube.dimensions = Vector( (max_dim - min_dim) ) * 1.05
        
        bpy.ops.object.select_all(action='DESELECT')
        self.cube.select = True
        bpy.ops.view3d.camera_to_view_selected()


@persistent
def scene_render_pre_handler( dummy ):    # <--- yes, really type dummy here
    print("Prerender")

def register():
     bpy.utils.register_class(RenderOp)

if __name__ == "__main__":
    register()
    camera = bpy.data.objects["Camera"] 
    if RenderOp.orig_loc is None:
        RenderOp.orig_loc = camera.location
    if RenderOp.orig_rot is None:
        RenderOp.orig_rot = camera.rotation_euler
    
    bpy.ops.wm.render_op(file_prefix="human", render_path = r"D:\makehuman\test", import_path = r"D:\makehuman\test\man")

