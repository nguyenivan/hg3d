import torch.utils.data as data
import numpy as np
import ref
import torch
import cv2
from h5py import File
from utils.utils import Rnd, Flip, ShuffleLR
from utils.img import Crop, DrawGaussian, Transform


# Things to do:
# - size of Strenxt in Ref ok
# - annot.h5 for Strenxt ok
# - imgDirStrenxt in Ref ok
# - Need to set ratio of Strenxt to 2D and 3D too

MPII_TO_STRENXT_RATIO = 20.0/40.0 
M36M_TO_STRENXT_RATIO = 20.0/40.0 

class FusionPose(data.Dataset):
  def __init__(self, opt, split):
    self.ratio3D = opt.ratio3D
    self.split = split
    self.dataset3D = H36MPose(opt, split)
    self.dataset2D = MPIIPose(opt, split, returnMeta = True)
    self.datasetMy = StrenxtPose(opt, split)
    self.nImagesMy = len(self.datasetMy)
    self.nImages2D = min(len(self.dataset2D), int(self.nImagesMy * MPII_TO_STRENXT_RATIO))
    self.nImages3D = min(len(self.dataset3D), int(self.nImagesMy * M36M_TO_STRENXT_RATIO))

    print '#Images2D {}, #Images3D {}, #ImagesMy {}'.format(self.nImages2D, self.nImages3D, self.nImagesMy)
  
  def __getitem__(self, index):
    if index < self.nImages3D:  
      return self.dataset3D[index]
    elif index < self.nImages3D + self.nImages2D:
      return self.dataset2D[index - self.nImages3D]
    else:
      return self.datasetMy[index - self.nImages2D - self.nImages3D]
    
  def __len__(self):
    return self.nImages2D + self.nImages3D + self.nImagesMy


class H36MPose(data.Dataset):
  def __init__(self, opt, split):
    print '==> initializing 3D {} data.'.format(split)
    annot = {}
    tags = ['action', 'bbox', 'camera', 'id', 'joint_2d', 'joint_3d_mono', 'subaction', 'subject', 'istrain']
    f = File('../data/h36m/annotSampleTest.h5', 'r')
    for tag in tags:
      annot[tag] = np.asarray(f[tag]).copy()
    f.close()
    
    ids = np.arange(annot['id'].shape[0])[annot['istrain'] == (1 if split == 'train' else 0)]
    for tag in tags:
      annot[tag] = annot[tag][ids]
    
    self.root = 7
    self.split = split
    self.opt = opt
    self.annot = annot
    self.nSamples = len(self.annot['id'])

    # WE ASSUME ALL IMAGES FROM THIS DATASET IS OF WRONG POSES
    self.outputs = np.zeros(self.nSamples)
    # self.outputs[np.random.permutation(self.nSamples)] = 1 
    
    print 'Loaded 3D {} {} samples'.format(split, len(self.annot['id']))
  
  def LoadImage(self, index):
    folder = 's_{:02d}_act_{:02d}_subact_{:02d}_ca_{:02d}'.format(self.annot['subject'][index], self.annot['action'][index], \
              self.annot['subaction'][index], self.annot['camera'][index])
    path = '{}/{}/{}_{:06d}.jpg'.format(ref.h36mImgDir, folder, folder, self.annot['id'][index])
    #print 'path', path
    img = cv2.imread(path)
    return img
  
  def __getitem__(self, index):
    if self.split == 'train':
      index = np.random.randint(self.nSamples)
    img = self.LoadImage(index)
    c = np.ones(2) * ref.h36mImgSize / 2
    s = ref.h36mImgSize * 1.0
      
    inp = Crop(img, c, s, 0, ref.inputRes) / 256.
    # change here
    outp = self.outputs[index]
    inp = torch.from_numpy(inp)
    return inp, outp
    
  def __len__(self):
    return self.nSamples


class MPIIPose(data.Dataset):
  def __init__(self, opt, split, returnMeta = False):
    print '==> initializing 2D {} data.'.format(split)
    annot = {}
    tags = ['imgname','part','center','scale']
    f = File('{}/mpii/annot/{}.h5'.format(ref.dataDir, split), 'r')
    for tag in tags:
      annot[tag] = np.asarray(f[tag]).copy()
    f.close()

    print 'Loaded 2D {} {} samples'.format(split, len(annot['scale']))
    
    self.split = split
    self.opt = opt
    self.annot = annot
    self.returnMeta = returnMeta
    self.nSamples = len(self.annot['scale'])
    # WE ASSUME ALL IMAGES FROM THIS DATASET IS OF WRONG POSES
    self.outputs = np.zeros(self.nSamples)
    # self.outputs[np.random.permutation(self.nSamples)] = 1 
  
  def LoadImage(self, index):
    path = '{}/{}'.format(ref.mpiiImgDir, self.annot['imgname'][index])
    img = cv2.imread(path)
    return img
  
  def GetPartInfo(self, index):
    pts = self.annot['part'][index].copy()
    
    return pts, c, s
      
  def __getitem__(self, index):
    img = self.LoadImage(index)
    c = self.annot['center'][index].copy()
    s = self.annot['scale'][index]
    s = s * 200
    r = 0
    
    if self.split == 'train':
      s = s * (2 ** Rnd(ref.scale))
      r = 0 if np.random.random() < 0.6 else Rnd(ref.rotate)
    try:
      inp = Crop(img, c, s, r, ref.inputRes) / 256.
    except: #MrWin
      path = '{}/{}'.format(ref.mpiiImgDir, self.annot['imgname'][index])
      print (path)
      raise

    if self.split == 'train':
      if np.random.random() < 0.5:
        inp = Flip(inp)
      #print 'before', inp[0].max(), inp[0].mean()
      inp[0] = np.clip(inp[0] * (np.random.random() * (0.4) + 0.6), 0, 1)
      inp[1] = np.clip(inp[1] * (np.random.random() * (0.4) + 0.6), 0, 1)
      inp[2] = np.clip(inp[2] * (np.random.random() * (0.4) + 0.6), 0, 1)
      #print 'after', inp[0].max(), inp[0].mean()
    
    outp = self.outputs[index]
    inp = torch.from_numpy(inp)
    return inp, outp
    
  def __len__(self):
    return self.nSamples

class StrenxtPose(data.Dataset):
  def __init__(self, opt, split, returnMeta = False):
    print '==> initializing Strenxt {} data.'.format(split)
    annot = {}
    tags = ['imgname', 'split', 'target']
    f = File('{}/look_phone/annot.h5'.format(ref.dataDir), 'r')
    for tag in tags:
      annot[tag] = np.asarray(f[tag]).copy()
    f.close()
    idx = (annot['split'] == split)
    for tag in tags:
      annot[tag] = annot[tag][idx]
    print 'Loaded Strenxt {} {} samples'.format(split, len(annot['imgname']))

    self.split = split
    self.opt = opt
    self.annot = annot
    self.returnMeta = returnMeta
    self.nSamples = len(self.annot['imgname'])
    # WE ASSUME ALL IMAGES FROM THIS DATASET IS OF THE RIGHT POSE
    self.outputs = self.annot['target']
  
  def LoadImage(self, index):
    path = '{}/{}'.format(ref.strenxtImgDir, self.annot['imgname'][index])
    img = cv2.imread(path)
    return img
  
  def __getitem__(self, index):
    img = self.LoadImage(index)
    c = np.ones(2) * ref.strenxtImgSize / 2
    s = ref.strenxtImgSize * 1.0
    r = 0
    try:
      inp = Crop(img, c, s, 0, ref.inputRes) / 256.
    except: #MrWin
      path = '{}/{}'.format(ref.strenxtImgDir, self.annot['imgname'][index])
      print (path)
      raise
    inp = torch.from_numpy(inp)
    outp = self.outputs[index]

    return inp, outp
    
  def __len__(self):
    return self.nSamples