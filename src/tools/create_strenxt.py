import h5py as h5
import os
import numpy as np

img_files = list(filter(lambda x: x.endswith('.png'), os.listdir('.')))
# img_files = np.random.permutation(img_files)
n = len(img_files)
split_list = []
target_list = []
for i in range(n - 2):
    split = 'train'
    split_list.append(split)
    if  img_files[i].startswith('batch1_0'):
        target_list.append(0)
    else:
        target_list.append(1)
    print ("{} ==> {}".format(img_files[i], split))

for i in range(2):
    split = 'val'
    split_list.append(split)
    if  img_files[n-2+i].startswith('batch1_0'):
        target_list.append(0)
    else:
        target_list.append(1)
    print ("{} ==> {}".format(img_files[n-2+i], split))    

print (img_files)
print (split_list)
print (target_list)

file_name = 'annot.h5'

with h5.File(file_name,'w') as f:
    f.create_dataset('imgname', data=np.array(img_files, dtype='S'))
    f.create_dataset('split', data=np.array(split_list, dtype='S'))
    f.create_dataset('target', data=np.array(target_list))