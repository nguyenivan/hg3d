import torch
import numpy as np
from utils.utils import AverageMeter
from utils.eval import SimpleAccuracy
from utils.debugger import Debugger
from models.layers.FusionCriterion import FusionCriterion
import cv2
import ref
from utils.progress import progress
import IPython



def step(split, epoch, opt, dataLoader, model, criterion, optimizer = None):
  if split == 'train':
    model.train()
  else:
    model.eval()
  Loss, Acc = AverageMeter(), AverageMeter()
  # It's late but this will work: https://discuss.pytorch.org/t/equivalent-of-tensorflows-sigmoid-cross-entropy-with-logits-in-pytorch/1985/10
  nIters = len(dataLoader)
  out = IPython.core.display.display(progress(0, nIters), display_id=True)

  for i, (input, target) in enumerate(dataLoader):
    input_var = torch.autograd.Variable(input).float().cuda()
    target_var = torch.autograd.Variable(target).long().cuda()
    
    output = model(input_var)
    pose = output[-1]
    loss = criterion(pose, target_var)
    acc = SimpleAccuracy(pose.data.cpu().numpy(), target_var.data.cpu().numpy())
    Loss.update(loss.data[0], input.size(0))
    Acc.update( acc, input.size(0))
    if split == 'train':
      optimizer.zero_grad()
      loss.backward()
      optimizer.step()
    out.update(progress( i, nIters, "{} - Loss: {}, Acc: {}. Iter {}/{}".format(split, Loss.avg, Acc.avg, i+1, nIters)))

  return Loss.avg, Acc.avg
  

def train(epoch, opt, train_loader, model, criterion, optimizer):
  return step('train', epoch, opt, train_loader, model, criterion, optimizer)
  
def val(epoch, opt, val_loader, model, criterion):
  return step('val', epoch, opt, val_loader, model, criterion)
