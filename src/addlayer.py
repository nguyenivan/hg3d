"""addlayer.py: Transfer learning from stacked hourglass network."""
__author__      = "Nguyen D. Le"
__copyright__   = "Copyright 2022, Sifu.art"

import sys
import torch
from opts import opts
import ref
from utils.debugger import Debugger
from utils.eval import getPreds
import cv2
import numpy as np
from datasets.pose import FusionPose, H36Pose, MPIIPose
from transfertrain import train, val
from torch import nn



def main():
  opt = opts().parse()

  model_file = 'hgreg-3d.pth'
  model = torch.load('hgreg-3d.pth').cuda()

  # Freeze params up to reg_ layers. for the final FC layer we will change it later
  for name1, child in model.named_children():
    for name2, params in child.named_parameters():
      if name1 != 'reg_':
        params.requires_grad = False

  reg_feats = model.reg.in_features
  n_cls = 2
  model.reg = nn.Linear(reg_feats, n_cls, requires_grad = True).cuda()
  # Now apply the Cross Entropy Loss
  criterion = nn.CrossEntropyLoss().cuda()
  optimizer = torch.optim.RMSprop(filter(lambda p: p.requires_grad,model.parameters()), 2.5e-4, 
                                    alpha = ref.alpha, 
                                    eps = ref.epsilon, 
                                    weight_decay = ref.weightDecay, 
                                    momentum = ref.momentum)
  
  if opt.ratio3D < ref.eps:
    val_loader = torch.utils.data.DataLoader(
        MPII(opt, 'val', returnMeta = True), 
        batch_size = 1, 
        shuffle = False,
        num_workers = int(ref.nThreads)
    )
  else:
    val_loader = torch.utils.data.DataLoader(
        H36M(opt, 'val'), 
        batch_size = 1, 
        shuffle = False,
        num_workers = int(ref.nThreads)
    )
    
  train_loader = torch.utils.data.DataLoader(
        Fusion(opt, 'train'), 
        batch_size = 1, 
        shuffle = True if opt.DEBUG == 0 else False,
        num_workers = int(ref.nThreads)
    )
    
  for epoch in range(1, opt.nEpochs + 1):
      loss_train, acc_train = train(epoch, opt, train_loader, model, criterion, optimizer)

if __name__ == '__main__':
  main()
