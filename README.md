# Transfered Learning from 3D Human Pose Estimation to Score Human Posture

This repository is the PyTorch implementation for the Stack Hourglass network

Contact: [contact@sifu.art](contact@sifu.art)

## Requirements
- cudnn
- [PyTorch](http://pytorch.org/)
- Python with h5py, opencv and [progress](https://anaconda.org/conda-forge/progress)
- Optional: [tensorboard](https://www.tensorflow.org/get_started/summaries_and_tensorboard) 

