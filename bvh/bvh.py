import math
SCALE = 0.01
FPS = 0.033333


JointType = [
    "JOINT_HEAD",
    "JOINT_NECK",
    "
    "JOINT_LEFT_SHOULDER",
    "JOINT_RIGHT_SHOULDER",
    "JOINT_LEFT_ELBOW",
    "JOINT_RIGHT_ELBOW",
    "JOINT_LEFT_HAND",
    "JOINT_RIGHT_HAND",
    "
    "JOINT_TORSO",
    "
    "JOINT_LEFT_HIP",
    "JOINT_RIGHT_HIP",
    "JOINT_LEFT_KNEE",
    "JOINT_RIGHT_KNEE",
    "JOINT_LEFT_FOOT",
    "JOINT_RIGHT_FOOT",
    "JOINT_SIZE
]